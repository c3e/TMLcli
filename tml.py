#!/usr/bin/env python3
# TanteMate CLI
# V 0.1 with basic features (list drinks|users, buy stuff w/o account login)
import click
from os.path import expanduser
from getpass import getpass
from tabulate import tabulate
import requests
import json


try:
    with open(expanduser('~/.config/tantemate.conf'), 'r') as configfile:
        config = json.load(configfile)
except FileNotFoundError:
    config = {}
    config['BASE_URL'] = "https://tantemate.chaospott.de/"
    config['APIKEY'] = None
    config['USER'] = None


DRINKS = requests.get(config['BASE_URL']+'api/drinks/').json()
USERS = requests.get(config['BASE_URL']+'api/accounts/').json()

def getUser(name):
    return next(u for u in USERS if u["user"] == name)


def getCSRF():
    # Obtain a CSRF token
    return requests.get(config['BASE_URL']).cookies['csrftoken']

@click.group()
def cli():
    pass

@cli.command()
#@click.argument('drinks', required=False)
@click.argument('show', required=False, type=click.Choice(['drinks', 'users']))
def list(show):
    if show == "drinks" or show is None:
        drink_tbl = []
        for drink in DRINKS:
            drink_tbl.append([drink['slug'], drink['name'], drink['price'], drink['stock']])

        click.echo(tabulate(drink_tbl, headers=['Slug', 'Getränk','Preis (€)', "Vorrätig"]))

        if show is None:
            click.echo("")


    if show == "users" or show is None:
        user_tbl = []
        for user in USERS:
            user_tbl.append([user['id'], user['user'], user['free_access']])

        click.echo(tabulate(user_tbl, headers=['ID','Name', 'Freier Zugriff']))

@cli.command()
@click.argument('item', required=False, type=click.Choice([x['slug'] for x in DRINKS]))
@click.option('--user', '-u', prompt='Username', help='Buying user', default=config['USER'])
@click.option('--count', default=1, help='Number of items to buy.')
def buy(item, user, count):
    if item is None:
        i = 0
        tbl = []
        for drink in DRINKS:
            tbl.append([i, drink['name'],drink['slug']])
            i += 1
        click.echo(tabulate(tbl, headers=['ID','Name', 'Slug']))
        itemid = int(click.prompt("Drink-ID"))
        item = DRINKS[itemid]['slug']
    headers = {'X-CSRFToken':getCSRF()}

    if user == config['USER'] and config['APIKEY']:
        headers['Authorization'] = "Token %s" % config['APIKEY']
        data = None

    elif getUser(user)['free_access']:
        data = None
    else:
        pin = getpass(prompt="PIN:")
        data = {'pin':pin}
    r =requests.post(config['BASE_URL']+"api/buy/item/%s/%s/" % (getUser(user)['id'], item), data=data, headers=headers)
    print(r.json())

@cli.command()
@click.option('--user', '-u', prompt='Username', default=config['USER'])
@click.option('--api-key', '-api', prompt='API Key', default=config['APIKEY'])
@click.option('--hostname', '-h', prompt='Hostname', default=config['BASE_URL'])
def create_config(user,api_key,hostname):
    with open(expanduser('~/.config/tantemate.conf'), 'w+') as configfile:
        config = {}
        config['BASE_URL'] = hostname
        config['APIKEY'] = api_key
        config['USER'] = user
        config = json.dump(config,configfile)
    print('Config saved to: ~/.config/tantemate.conf')
if __name__ == "__main__":
    cli()